package com.hackathon.rappi.baseDatosMongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseDatosMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseDatosMongoApplication.class, args);
	}
}
