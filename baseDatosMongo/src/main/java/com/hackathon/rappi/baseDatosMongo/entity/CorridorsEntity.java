package com.hackathon.rappi.baseDatosMongo.entity;

import java.io.Serializable;

public class CorridorsEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String parent_id;
	private int id;
	private String name;
	
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	



	
}
