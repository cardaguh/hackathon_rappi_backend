package com.hackathon.rappi.baseDatosMongo.entity;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "products")
public class ProductEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	private Object  id;
	private String name;
	private String image;
	private String description;
	private int store_id;
	private int price;
	
	@Field
	private List<CorridorsEntity> corridors;
	private int product_id;
	
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStore_id() {
		return store_id;
	}
	public void setStore_id(int store_id) {
		this.store_id = store_id;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public List<CorridorsEntity> getCorridors() {
		return corridors;
	}
	public void setCorridors(List<CorridorsEntity> corridors) {
		this.corridors = corridors;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	

}
