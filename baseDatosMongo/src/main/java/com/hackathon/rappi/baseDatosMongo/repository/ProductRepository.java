package com.hackathon.rappi.baseDatosMongo.repository;

import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.rappi.baseDatosMongo.entity.ProductEntity;

@Repository
public interface ProductRepository extends MongoRepository<ProductEntity, Serializable> {
	
}
