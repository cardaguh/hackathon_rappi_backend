package com.hackathon.rappi.baseDatosMongo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.rappi.baseDatosMongo.entity.ProductEntity;
import com.hackathon.rappi.baseDatosMongo.repository.ProductRepository;


@RestController
@RequestMapping("/Product")
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	
	@GetMapping("/consultarProducts")
	public List<ProductEntity> consultarLosVehiculosParqueados(){
		return productRepository.findAll();
	}
	

}
