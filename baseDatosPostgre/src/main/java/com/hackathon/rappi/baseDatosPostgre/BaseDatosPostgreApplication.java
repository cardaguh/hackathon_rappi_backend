package com.hackathon.rappi.baseDatosPostgre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseDatosPostgreApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseDatosPostgreApplication.class, args);
	}
}
