package com.hackathon.rappi.baseDatosPostgre.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "corridors")
public class CorridorsEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(nullable = false)
	private int id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String icon;
	
	@Column(nullable = false)
	private int index;
	
	@Column(nullable = false)
	private int type;
	
	
	@Column(nullable = false)
	private Date created_at;
	
	@Column(nullable = false)
	private Date updated_at;
	
	@Column(nullable = false)
	private Date deleted_at;
	
	@Column(nullable = true)
	private String struct_id;
	
	@Column(nullable = false)
	private int device;
	
	@Column(nullable = false)
	private String description;
	
	@Column(nullable = false)
	private boolean has_antismoking;
	
	@Column(nullable = false)
	private String icon_grid;
	
	@Column(nullable = false)
	private String image_big;
	
	@Column(nullable = false)
	private String image_small;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public Date getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Date deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getStruct_id() {
		return struct_id;
	}

	public void setStruct_id(String struct_id) {
		this.struct_id = struct_id;
	}

	public int getDevice() {
		return device;
	}

	public void setDevice(int device) {
		this.device = device;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isHas_antismoking() {
		return has_antismoking;
	}

	public void setHas_antismoking(boolean has_antismoking) {
		this.has_antismoking = has_antismoking;
	}

	public String getIcon_grid() {
		return icon_grid;
	}

	public void setIcon_grid(String icon_grid) {
		this.icon_grid = icon_grid;
	}

	public String getImage_big() {
		return image_big;
	}

	public void setImage_big(String image_big) {
		this.image_big = image_big;
	}

	public String getImage_small() {
		return image_small;
	}

	public void setImage_small(String image_small) {
		this.image_small = image_small;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
