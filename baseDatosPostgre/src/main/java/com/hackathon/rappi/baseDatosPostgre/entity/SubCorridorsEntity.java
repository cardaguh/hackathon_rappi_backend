package com.hackathon.rappi.baseDatosPostgre.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sub_corridors")
public class SubCorridorsEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false)
	private int  id;
	
	@Column(nullable = false)
	private int corridor_id;
	
	@Column(nullable = false)
	private int type;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String icon;
	
	@Column(nullable = false)
	private int  index;
	
	@Column(nullable = false)
	private String background;
	
	@Column(nullable = false)
	private Date created_at;
	
	@Column(nullable = false)
	private Date updated_at;
	
	@Column(nullable = false)
	private Date deleted_at;
	
	@Column(nullable = true)
	private String struct_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCorridor_id() {
		return corridor_id;
	}

	public void setCorridor_id(int corridor_id) {
		this.corridor_id = corridor_id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public Date getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Date deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getStruct_id() {
		return struct_id;
	}

	public void setStruct_id(String struct_id) {
		this.struct_id = struct_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	
}
