package com.hackathon.rappi.baseDatosPostgre.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.hackathon.rappi.baseDatosPostgre.entity.CorridorsEntity;

@Repository
public interface CorridorsRepository extends JpaRepository<CorridorsEntity, Serializable> {

	@Query("select p from CorridorsEntity c where c.id = :id")
	List<CorridorsEntity> searchCorridorsId (@Param("id") int id);
}
