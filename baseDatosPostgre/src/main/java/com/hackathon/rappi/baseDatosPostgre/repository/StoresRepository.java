package com.hackathon.rappi.baseDatosPostgre.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.hackathon.rappi.baseDatosPostgre.entity.StoresEntity;

@Repository
public interface StoresRepository extends JpaRepository<StoresEntity, Serializable> {

	@Query("select p from CorridorsEntity c where c.store_id = :store_id")
	List<StoresEntity> searchCorridorsId (@Param("store_id") int store_id);
}
