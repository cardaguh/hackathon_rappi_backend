package com.hackathon.rappi.baseDatosPostgre.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.hackathon.rappi.baseDatosPostgre.entity.SubCorridorsEntity;

@Repository
public interface SubCorridorsRepository extends JpaRepository<SubCorridorsEntity, Serializable> {

	@Query("select p from SubCorridorsEntity s where s.id = :id")
	List<SubCorridorsEntity> searchSubCorridorsId (@Param("id") int id);
}
