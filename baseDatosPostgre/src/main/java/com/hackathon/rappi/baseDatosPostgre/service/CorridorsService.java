package com.hackathon.rappi.baseDatosPostgre.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.rappi.baseDatosPostgre.entity.CorridorsEntity;
import com.hackathon.rappi.baseDatosPostgre.repository.CorridorsRepository;

@RestController
@RequestMapping("/corridors")
public class CorridorsService {
	
	@Autowired
	private CorridorsRepository corridorsRepository;
	
	@GetMapping("/consultarCorridors")
	public List<CorridorsEntity> consultarAllCorridors(){
		return corridorsRepository.findAll();
	}
	
	@PostMapping("/consultarCorridorsRecommended")
	public List<CorridorsEntity> consultarCorridorsRecommended(@RequestBody int id){
		return corridorsRepository.searchCorridorsId(id);
	}

}
