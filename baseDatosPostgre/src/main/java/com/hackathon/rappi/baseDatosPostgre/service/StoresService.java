package com.hackathon.rappi.baseDatosPostgre.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.rappi.baseDatosPostgre.entity.StoresEntity;
import com.hackathon.rappi.baseDatosPostgre.repository.StoresRepository;

@RestController
@RequestMapping("/stores")
public class StoresService {
	
	@Autowired
	private StoresRepository storesRepository;
	
	@GetMapping("/consultarStores")
	public List<StoresEntity> consultarAllStores(){
		return storesRepository.findAll();
	}
	
}
