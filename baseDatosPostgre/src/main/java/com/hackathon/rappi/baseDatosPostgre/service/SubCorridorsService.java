package com.hackathon.rappi.baseDatosPostgre.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.rappi.baseDatosPostgre.entity.SubCorridorsEntity;
import com.hackathon.rappi.baseDatosPostgre.repository.SubCorridorsRepository;

@RestController
@RequestMapping("/subCorridors")
public class SubCorridorsService {
	
	@Autowired
	private SubCorridorsRepository subCorridorsRepository;
	
	@GetMapping("/consultarSubCorridors")
	public List<SubCorridorsEntity> consultarAllSubCorridors(){
		return subCorridorsRepository.findAll();
	}
	
	@PostMapping("/consultByIdSubCorridors")
	public List<SubCorridorsEntity> consultByIdSubCorridors(@RequestBody int id){
		return subCorridorsRepository.searchSubCorridorsId(id);
	}

}
