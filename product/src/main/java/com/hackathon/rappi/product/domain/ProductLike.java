package com.hackathon.rappi.product.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.hackathon.rappi.product.entity.ProductEntity;
import com.hackathon.rappi.product.repository.ProductRepository;

@Repository("com.hackathon.rappi.product.domain")
public class ProductLike {
		
	@Autowired
	@Qualifier("com.hackathon.rappi.product.repository.ProductRepository")
	private ProductRepository productRepository; 
	
	public void saveProductWithUser(List<ProductEntity> products) {
		for (ProductEntity product : products){
			if(!validationUser(product.getIdUser(),product.getIdProduct())){
				productRepository.save(product);
			}
		}
	}
	
	public boolean validationUser (String idUser, String idProduct) {
		return (productRepository.searchProductWithUser(idUser , idProduct) != null);
	}
	

}
