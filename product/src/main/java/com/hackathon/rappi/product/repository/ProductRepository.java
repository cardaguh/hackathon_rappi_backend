package com.hackathon.rappi.product.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hackathon.rappi.product.entity.ProductEntity;


@Repository("com.hackathon.rappi.product.repository.ProductRepository")
public interface ProductRepository extends JpaRepository<ProductEntity, Serializable> {
	
	@Query("select p from ProductEntity p where p.idUser = :idUser and p.idProduct = :idProduct")
	ProductEntity searchProductWithUser (@Param("idUser") String idUser ,@Param("idProduct") String idProduct);

	@Query("select p from ProductEntity p where p.idUser = :idUser")
	List<ProductEntity> searchProductWithUserLike (@Param("idUser") String idUser);
}
