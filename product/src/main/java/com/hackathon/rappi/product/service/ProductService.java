package com.hackathon.rappi.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hackathon.rappi.product.domain.ProductLike;
import com.hackathon.rappi.product.entity.ProductEntity;
import com.hackathon.rappi.product.repository.ProductRepository;


@RestController
@RequestMapping("/productLike")
public class ProductService {
	
	
	@Autowired 
	@Qualifier("com.hackathon.rappi.product.domain")
	private ProductLike productLike;
	
	@Autowired
	@Qualifier("com.hackathon.rappi.product.repository.ProductRepository")
	private ProductRepository productRepository; 
	
	@PostMapping("/insertProductLoveUser")
	public void insertProductLoveUser(@RequestBody List<ProductEntity> products) throws Exception {
		productLike.saveProductWithUser(products);	
	}
	
	@PostMapping("/consutlProductLike")
	public List<ProductEntity> consutlProductLike(@RequestBody String idUser){
		return productRepository.searchProductWithUserLike(idUser);
	}

}
